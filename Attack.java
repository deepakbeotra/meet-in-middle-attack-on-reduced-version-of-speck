/**
 *
 * @Filename Attack.java
 *
 * @Version $Id: Attack.java,v 1.0 2014/24/07 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import edu.rit.util.Hex;
import edu.rit.util.Packing;

/**
 * The class is an attack on the reduced round version of
 * Speck32/64 algorithm reduced to 3 rounds
 * 
 * @author Deepak Mahajan
 * 
 */
public class Attack {

	// possible sub keys 1
	short[] subKey1 = new short[65536];
	
	// possible sub keys 2
	short[] subKey2 = new short[65536];
	
	// possible sub keys 3
	short[] subKey3 = new short[65536];

	/**
	 * 
	 * Default Constructor
	 * 
	 */
	Attack() {

	}

	/**
	 * 
	 * This function attacks 3 rounds of speck32/64
	 * algorithm by using Meet-In-The-Middle attack
	 * for just first oracle
	 * 
	 * @param plainText
	 * @param cipherText
	 */
	public void attack(String plainText, String cipherText) {
		
		// Check for all sub keys 1
		for (int i = 0; i < 65536; i++) {
			
			// up to down
			byte[] plain = Hex.toByteArray(plainText);
			subKey1[i] = (short) (i);
			short plainLeft = Packing.packShortBigEndian(plain, 0);
			short plainRight = Packing.packShortBigEndian(plain, 2);
			
			// Calculating intermediate states.
			short sA1 = (short) (((plainLeft >>> 7) & 0x1ff) | (plainLeft << 9));
			short sL1 = (short) (sA1 + plainRight);
			short sB1 = (short) ((plainRight << 2) | (plainRight >>> 14) & 0x3);
			short m1 = (short) (subKey1[i] ^ sL1);
			short sR2 = (short) (m1 ^ sB1);
			short sA2 = (short) (((m1 >>> 7) & 0x1ff) | m1 << 9);
			short sB2 = (short) (((sR2 >>> 14) & 3) | sR2 << 2);

			// down to up
			byte[] cipher = Hex.toByteArray(cipherText);
			short cipherLeft = Packing.packShortBigEndian(cipher, 0);

			short cipherRight = Packing.packShortBigEndian(cipher, 2);

			// Calculating intermediate values
			short sB3 = (short) (cipherLeft ^ cipherRight);
			short sR3 = (short) ((sB3 >>> 2) & 0x3fff | (sB3 << 14));
			short sL3 = (short) (sR3 ^ sB2);
			
			// Calculating sub key 2
			subKey2[i] = (short) ((sA2 + sR2) ^ sL3);
			short sA3 = (short) (((sL3 >>> 7) & 0x1ff) | sL3 << 9);
			
			// Calculating sub key 3
			subKey3[i] = (short) ((sA3 + sR3) ^ cipherLeft);

		}
	}

	/**
	 * 
	 * This function attacks 3 rounds of speck32/64
	 * algorithm by using Meet-In-The-Middle attack
	 * for all the oracles other than 1
	 * 
	 * @param plainText
	 * @param cipherText
	 * @param flag
	 */
	public int[] attack(String plainText, String cipherText, Attack atk,
			int[] flag) {
		
		// Brute force for sub key 1
		for (int i = 0; i < 65536; i++) {
			if (flag[i] == 0) {
				
				// up to down
				byte[] plain = Hex.toByteArray(plainText);
				subKey1[i] = (short) (i);
				short plainLeft = Packing.packShortBigEndian(plain, 0);
				short plainRight = Packing.packShortBigEndian(plain, 2);
				
				// Calculating intermediate values
				short sA1 = (short) (((plainLeft >>> 7) & 0x1ff) | (plainLeft << 9));
				short sL1 = (short) (sA1 + plainRight);
				short sB1 = (short) ((plainRight << 2) | (plainRight >>> 14) & 0x3);
				short m1 = (short) (subKey1[i] ^ sL1);
				short sR2 = (short) (m1 ^ sB1);
				short sA2 = (short) (((m1 >>> 7) & 0x1ff) | m1 << 9);
				short sB2 = (short) (((sR2 >>> 14) & 3) | sR2 << 2);

				// down to up

				// Left 16 bits of input
				byte[] cipher = Hex.toByteArray(cipherText);
				short cipherLeft = Packing.packShortBigEndian(cipher, 0);

				// Right 16 bits of input
				short cipherRight = Packing.packShortBigEndian(cipher, 2);

				// Calculating intermediate values
				short sB3 = (short) (cipherLeft ^ cipherRight);
				short sR3 = (short) ((sB3 >>> 2) & 0x3fff | (sB3 << 14));

				short sL3 = (short) (sR3 ^ sB2);
				
				// sub key 2 calculated
				subKey2[i] = (short) ((sA2 + sR2) ^ sL3);
				
				short sA3 = (short) (((sL3 >>> 7) & 0x1ff) | sL3 << 9);
				
				// sub key 3 calculated 
				subKey3[i] = (short) ((sA3 + sR3) ^ cipherLeft);
				
				// check if keys are same as previous
				if (atk.subKey2[i] != subKey2[i]
						|| atk.subKey3[i] != subKey3[i]) {
					flag[i] = 1;
				}
			}
		}
		return flag;
	}

	/**
	 * 
	 * This is the main function
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			
			// check is file name given
			if (args.length == 1) {
				
				// flag to check which keys are common for all oracles
				int flag[] = new int[65536];
				
				// File to be read
				File file = new File(args[0]);
				Scanner sc = new Scanner(file);
				
				// Plain texts given by oracle
				ArrayList<String> plain = new ArrayList<String>();
				
				// Cipher texts given by oracle
				ArrayList<String> cipher = new ArrayList<String>();
				
				// variable to check length of hexadecimal string
				boolean checkLength = true;
				
				// Oracle objects in an arraylist
				ArrayList<Attack> elements = new ArrayList<Attack>();
				while (sc.hasNext()) {
					String plainText = sc.next();
					String cipherText = sc.next();
					
					// check length of hexadecimal string
					if (plainText.length() != 8 || cipherText.length() != 8) {
						System.out.println("Usage Error: Invalid Input");
						checkLength = false;
						System.exit(1);
					} else {
						plain.add(plainText);
						cipher.add(cipherText);
						Attack atk = new Attack();
						elements.add(atk);
					}
				}
				
				// call attack
				if (checkLength) {
					for (int j = 0; j < plain.size(); j++) {
						if (j == 0) {
							elements.get(j).attack(plain.get(j), cipher.get(j));
						} else {
							flag = elements.get(j).attack(plain.get(j),
									cipher.get(j), elements.get(j - 1), flag);
						}
					}
					
					// print solution
					for (int i = 0; i < flag.length; i++) {
						if (flag[i] == 0) {
							System.out
									.println(Hex.toString(elements.get(0).subKey1[i]));
							System.out
									.println(Hex.toString(elements.get(0).subKey2[i]));
							System.out
									.println(Hex.toString(elements.get(0).subKey3[i]));
							break;
						}
					}
				}
			} else {
				System.out.println("Usage Error: Give <File Name>");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			System.out.println("Usage Error: Invalid Input");
		}
	}

}
